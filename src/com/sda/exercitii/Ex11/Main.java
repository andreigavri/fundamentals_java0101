package com.sda.exercitii.Ex11;
/*
Sa se citeasca de la tastatura 10 cuvinte care apoi sa fie salvate intr-un array.
Sa se afiseze pe ecran fiecare element din array impreuna cu dimensiunea elementului.
Exemplu: mere, pere, cirese
         mere - 4, pere - 4, cirese -6
 */

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] cuvinte = new String[10];

        for (int i = 0; i < 10; i++) {
            System.out.println("Adaugati cuvinte: ");
            String n = scanner.nextLine();
            cuvinte[i] = n;
        }

        for (int i = 0; i < 10; i++) {
            String cuvant = cuvinte[i];
            System.out.println(cuvant + " - " + cuvant.length());
        }
    }
}
