package com.sda.exercitii.Ex9;
/*
Se introduc de la tastatura 3 numere a,b si c.
Sa se afiseze cele 3 numere in ordine crescatoare adica daca introducem a = 5, b = 0  si c = 3 sa se afiseze 0 3 5.

 */

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Introduceti a: ");
        int a = scanner.nextInt();

        System.out.println("-----");

        System.out.println("Introduceti b: ");
        int b = scanner.nextInt();

        System.out.println("-----");
        System.out.println("Introduceti c: ");
        int c = scanner.nextInt();

        System.out.println("-----");

        //a = 5
        //b = 0
        //c = 3

//        int a1,b1,c1;

        if (a >= b) {
            int temp = a;
            a = b;
            b = temp;
        }

        if (a >= c) {
            int temp = a;
            a = c;
            c = temp;
        }

        if (b >= c) {
            int temp = b;
            b = c;
            c = temp;
        }

        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
    }
}
