package com.sda.exercitii.Ex6;
/* Sa se afiseze toate numerele de la 1 la 100 care sunt divizibile cu 3 apoi cu 5 iar la final cele care sunt divizibile cu amandoua(3 si 5)

 */

public class Main {
    public static void main(String[] args) {
        System.out.println("Numere dizibile cu 3:");
        for (int i = 1; i <= 100; i++) {
            if (i % 3 == 0) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
        System.out.println("Numere dizibile cu 5:");
        for (int i = 1; i <= 100; i++) {
            if (i % 5 == 0) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
        System.out.println("Numerele divizibile cu 3 si 5:");
        for (int i = 1; i <= 101; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.print(i + " ");
            }
        }
    }
}