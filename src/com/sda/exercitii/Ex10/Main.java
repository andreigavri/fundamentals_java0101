package com.sda.exercitii.Ex10;

import java.util.Scanner;

/*
Sa se introduca de la tastatura 10 numere intregi , pe care le vom salva intr-un array.
 Sa se introduca un numar x de la tastatura si sa se verifice daca numarul este in array , daca da sa se afiseze
 un mesaj : Numarul a fost gasti  altfel  Sa se afiseze : Numarul nu sa gasit.
 */
public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int[] numbers = new int[10];

        for (int i = 0; i < 10; i++) { //adaugam numere in array
            System.out.println("Introduceti numar: ");
            int n = scanner.nextInt();

            numbers[i] = n;
        }
        System.out.println("Introduceti x: ");
        int x = scanner.nextInt();
        for (int i = 0; i < 10; i++) { //parcurgem elementele pentru a verifica daca x este in array sau nu
            int m = numbers[i];// m ia valoarea elementului de pe pozitia i din array

            if (x == m) {
                System.out.println("Este in array");

            }
        }
    }
}
