package com.sda.loops;

import java.util.Scanner;

public class Exercises {
    public static void main(String[] args) {
        afisareAprimelor30deNumere();
//        sumaPrimelor5numereCititedelaTastatura();
//        afisareaAprimelor10numerePare();
//        afisareaprimelor300deNumereDivizibileCu3();
//        afisare10NumereDivizibilecu3si5();
//        afisareaNumerelordela0la100();

    }

    public static void afisareAprimelor30deNumere() {
        for (int i = 0; i < 30; i++) {
            System.out.println(i);
        }

        int i = 0;
        while (i < 30) {
            System.out.println(i);
            i++;
        }

        System.out.println("--------");
        System.out.println(i);

        i = 0;
        do {
            System.out.println("do while: " + i);
//          i++;
            i = i + 1;
        } while (i < 30);
    }

    public static void sumaPrimelor5numereCititedelaTastatura() {
        Scanner scanner = new Scanner(System.in); //Scanner cu s mare este tipul scanner cu s mic este obiectul
        int suma = 0;
        for (int i = 0; i < 5; i++) {
            System.out.println("introduceti numar:");
            int numar = scanner.nextInt();
            suma = suma + numar;
            System.out.println(suma);
        }
    }

    public static void afisareaAprimelor10numerePare() {
//        for(int i = 1; i < 22; i++){
//            if(i % 2 == 0) {
//                System.out.println(i);
//            }
//        }

        int i = 1;
        int contor = 0; //contorul tinem cont cate numere pare am gasit

        while (contor < 100) {
            if (i % 2 == 0) {
                System.out.println(i);
                contor++;
            }
            i++;
        }
    }

    public static void afisareaprimelor300deNumereDivizibileCu3() {
        int i = 3;
        int contor = 0;

        while (contor < 300) {
            if (i % 3 == 0) {
                System.out.println(i);
                contor++;
            }
            i++;
        }

    }

    public static void afisare10NumereDivizibilecu3si5() {
        int i = 3;
        int contor = 0;
        while (contor < 10) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.println(i);
                contor++;
            }
            i++;
        }
    }


    public static void afisareaNumerelordela0la100() {
        for (int i = 0; i < 100; i++) {
            System.out.println(i);
        }
        for (int i = 100; i > 0; i--) {
            System.out.println(i);
        }
    }
}
