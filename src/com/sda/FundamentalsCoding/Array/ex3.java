package com.sda.FundamentalsCoding.Array;
/*
Write a Java program to sort a string array in alphabetical order.
 */

import java.util.Arrays;

public class ex3 {
    public static void main(String[] args) {
        String[] alphabet = {"Ana","Floare","Bianca","Pix", "Geanta"};
        Arrays.sort(alphabet);
        System.out.println(Arrays.toString(alphabet));

        int[] numere = {7,6,10,3,8};
        Arrays.sort(numere);
        System.out.println(Arrays.toString(numere)); // important pentru a printa necesita sa folosim toString in sout 
    }
}
