package com.sda.FundamentalsCoding.Array;
/*
Write a Java program to calculate the average value of array elements. (edited)
 */

public class ex4 {
    public static void main(String[] args) {

        int[] elemente = {2, 3, 6, 7,5};

        double average = 0; // suma / numarul elementelor

        int suma = 0;
        int nrElemente = elemente.length;

        for( int i = 0; i < nrElemente; i++ ){

            suma += elemente[i];// asa trecem prin array nostru si lu am fiecare element

        }
        average = (double)suma / nrElemente;
        System.out.println(" Average is: " + average);

    }
}
