package com.sda.FundamentalsCoding.Array;

import com.sda.Main;

/*
Write a Java program to find the number of even and odd integers in a given array of integers:
[1, 7, 3, 10, 9]
Output: Odd=4; Even=1

 */
public class EX5 {
    public static void main(String[] args) {
       int[] numbers = {1,7,3,10,9};
       int nrPare = 0;
       int nrImpar = 0;
       for(int number: numbers){
           if(number % 2 == 0){
            nrPare++;// aici incrementam variabila nrPare care defapt tine evidenta la cate numere pare sunt in array
           }else{
               nrImpar++;
           }
       }
        System.out.println("Nr de elemente pare este: " + nrPare);
        System.out.println("Nr de elemente impare este: " + nrImpar);
    }
}
