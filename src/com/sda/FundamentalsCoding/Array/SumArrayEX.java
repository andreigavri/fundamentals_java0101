package com.sda.FundamentalsCoding.Array;

/*
Write a Java program to find the sum of all elements in an integer array.
 */
public class SumArrayEX {
    public static void main(String[] args) {

        int[] array1 = {3,5,6,9};
        int sum = 0;

//        for(int n: array1){ // asa parcurgem noi array ul de numere   ( similar cu int i = 0 ; i < 0; i++);
//            sum += n; // sum = sum + n
//        }
        for(int i = 0; i< array1.length; i++){
            sum+= array1[i]; //asa luam fiecare element
        }
        System.out.println("Suma este: " + sum);
    }
}
