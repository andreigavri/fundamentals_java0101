package com.sda.FundamentalsCoding.Array;
/*Write a Java program to find the maximum and minimum values in an array of type double.

 */

public class MaxMinArray {
    public static void main(String[] args) {
        double[] array1 = {3.4,10.4,6.8,1.5};
        double min = 3.4; // double min = array1[0]
        double max = 0.0;

        for(int i = 0; i < array1.length; i++){
            if(array1[i] > max) {
                max = array1[i];
            }
            if(array1[i] < min) {
                min = array1[i];
            }
        }
        System.out.println("Maxium este: " + max);
        System.out.println("Minimul este: " + min);
    }
}
