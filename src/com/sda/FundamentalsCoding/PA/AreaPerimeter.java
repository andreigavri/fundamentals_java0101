package com.sda.FundamentalsCoding.PA;

/*
Write a Java program to print the area and perimeter of a rectangle: Width = 5.5; Height = 8.5
Output: Area = 46.75; Perimeter = 28.0

 */

public class AreaPerimeter {
    public static void main(String[] args) {

        double w = 5.5;
        double h = 8.5;

        double area = w * h;
        System.out.println("Area = " + area);

        double perimeter = 2 * (w + h);
        System.out.println("Perimeter = " + perimeter);

    }
}
