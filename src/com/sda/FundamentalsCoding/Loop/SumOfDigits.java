package com.sda.FundamentalsCoding.Loop;

/**
 * Sum of digits
 * Write a method that takes a positive integer as input and calculates the sum of its digits. Return the sum as the output.
 * 1. Declare a method called calculateSumOfDigits that takes an integer number as input and
 * returns an integer representing the sum of its digits.
 * 2. Inside the method, initialize a variable called sum to store the sum of the digits. Set it to 0 initially.
 * 3. Create a loop using the while loop construct. The loop should continue as long as number is greater than 0.
 * 4. Within the loop, extract the rightmost digit of the number using the modulo operator %. The modulo operator returns
 * the remainder when the number is divided by 10. Assign this rightmost digit to a variable called digit.
 * 5. Add the value of digit to the sum variable.
 * 6. Update the value of number by dividing it by 10. This will remove the rightmost digit from the number.
 * 7. Repeat steps 4-6 until number becomes 0. Each iteration of the loop will process one digit of the original number.
 * 8. After the loop ends, return the value of sum as the result.
 */

public class SumOfDigits {
    public static int calculateSumOfDigits(int n) {
        int sum = 0;
        while (n > 0) {
            int digit = n % 10;
            sum = sum + digit; // sum +=digit
            n = n / 10;

        }
        return sum;
    }

    public static void main(String[] args) {
        System.out.println(calculateSumOfDigits(123));
    }
}
