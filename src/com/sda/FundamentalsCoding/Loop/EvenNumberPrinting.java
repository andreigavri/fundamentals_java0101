package com.sda.FundamentalsCoding.Loop;
/*
Even Number Printing
Write a Java method named printEvenNumbers that uses a for loop to print the even numbers from 1 to 20.
Each number should be printed on the same line, separated by a space.

 */

public class EvenNumberPrinting {
    public static void printEvenNumbers() {
        for (int i = 1; i <= 20; i++){
        if(i % 2 ==0) {
            System.out.print(i + " ");

            }
        }
    }

    public static void main(String[] args) {
        printEvenNumbers();
       
    }
}
