package com.sda.FundamentalsCoding.Loop;
/*
Multiplication table
Write a Java method named printMultiplicationTable that takes a number as a parameter
and uses a for loop to print its multiplication table up to 10.
Each line should display the multiplication expression and its result.
Example Output:
7 * 1 = 7
7 * 2 = 14
7 * 3 = 21
7 * 4 = 28
7 * 5 = 35
7 * 6 = 42
7 * 7 = 49
7 * 8 = 56
7 * 9 = 63
7 * 10 = 70
 */

public class MultiplicationTable {
    public static void printMultiplicationTable(int number) {
        for (int i = 0; i <= 10; i++) {
            int result = number * i;
            System.out.println(number + " * " + i + " = " + result);
        }
    }

    public static void main(String[] args) {
        int a = 7;
        printMultiplicationTable(a);

        System.out.println("------");

        int b = 10;
        printMultiplicationTable(b);
    }
}
