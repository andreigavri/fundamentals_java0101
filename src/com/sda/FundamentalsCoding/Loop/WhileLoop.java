package com.sda.FundamentalsCoding.Loop;
/*
Countdown
Write a method that takes an integer as input and counts down from that number to 1, printing each number on a new line.
Output Example:
input: 5
5
4
3
2
1
 */

public class WhileLoop {
    public static void countdown(int number) {

        while (number >= 1) {// aici puneam conditia de oprire, iar daca conditia nu este corecta se va repeta la infinit.
            System.out.println(number);
            number--;
        }
    }

    public static void main(String[] args) {
        countdown(5);
    }
}
