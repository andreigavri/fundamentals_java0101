package com.sda.FundamentalsCoding.Loop;

/*
Factorial Calculation

        Write a Java method named calculateFactorial that takes an integer n as a parameter and calculates the factorial of n.
        The factorial of a non-negative integer n is the product of all positive integers less than or equal to n.
        Display the calculated factorial value.
        Example Output
        Input: n = 5
        Output: Factorial of 5 is: 120
        In this example, we calculate the factorial of 5. The factorial of 5 is 5 * 4 * 3 * 2 * 1, which equals 120.
        The output statement displays the calculated factorial value as Factorial of 5 is: 120.

 */

public class FactorialCalculation {
    public static int calculateFactorial(int n) {
        int factorial = 1;

        for (int i = 1; i <= n; i++) {
            factorial = factorial * i;

        }
        return factorial;
    }

    public static void main(String[] args) {
        calculateFactorial(5);
        System.out.println("Factorial of 5 is " + calculateFactorial(5));
    }
}
