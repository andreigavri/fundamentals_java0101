package com.sda.FundamentalsCoding.Loop;

/*
Sum Calculation
Write a Java method named calculateSum that uses a for loop to calculate the sum of numbers from 1 to 100.
The method should return the final sum.

 */

public class SumaNumerelor {

    public static int calculateSum() {
        int suma = 0; // declaram variabilele la inceput, daca sunt declarate si initializate in for, sunt vizibile doar in for.

        for (int i = 1; i <= 100; i++) {
            suma += i; // (suma = suma + i)
//            System.out.println(suma);
        }
        return suma;
    }

    public static void main(String[] args) {
        calculateSum();
        int finalSum = calculateSum();
        System.out.println("Final Result = " + calculateSum());
    }
}

