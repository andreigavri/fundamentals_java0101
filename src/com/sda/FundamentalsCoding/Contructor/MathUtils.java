package com.sda.FundamentalsCoding.Contructor;
/*

Create a class called "MathUtils" with a method named "multiply" that multiplies two integers and returns the result.
Overload the "multiply" method to handle three integers and return their product.

 */

public class MathUtils {

    public static int multiply(int nr1, int nr2) {
        int result = nr1 * nr2;
        return result;
    }

    public static int multiply(int nr1, int nr2, int nr3) {
        int result = nr1 * nr2 * nr3;
        return result;
    }

    public static void main(String[] args) {
        System.out.println(multiply(3,5));
        System.out.println(multiply(1,6,3));
    }
}
