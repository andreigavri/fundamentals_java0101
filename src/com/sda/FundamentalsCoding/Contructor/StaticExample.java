package com.sda.FundamentalsCoding.Contructor;

// exemplu cu variabile statice
public class StaticExample {
    // normal field
    // variabile de instanta
    public int myNumber = 10;
    // static field
    // variabile de clasa
    public static int myStaticNumber = 15;
    public static void main(String[] args) {
        //System.out.println(StaticExample.myNumber); -> Nu se poate
        // Pot accesa variabila statica folsindu-ma de numele clasei
        System.out.println(StaticExample.myStaticNumber);
        // Daca vreau sa accesez myNumber, variabila non-statica
        // Am creat un obiect/instanta
        StaticExample staticExampleObject = new StaticExample();
        System.out.println(staticExampleObject.myNumber);
    }
}








