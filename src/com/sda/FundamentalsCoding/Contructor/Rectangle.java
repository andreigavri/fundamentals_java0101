package com.sda.FundamentalsCoding.Contructor;
/*
Create a class called "Rectangle" with attributes for length and width.
Add methods to calculate the area and perimeter of the rectangle.
Create an object of the Rectangle class, set values for length and width, and print the calculated area and perimeter.

 */

import java.sql.SQLOutput;

public class Rectangle {

    int lungime;
    int latime;

    public Rectangle(int lungime, int latime){
        this.lungime = lungime;
        this.latime = latime;
    }
    public int calculeazaAria(){
        int aria = latime * lungime;
        return aria;
    }
    public int calculeazaPerimetru(){
        int perimetru = 2*(lungime + latime);
        return perimetru;

    }

    public static void main(String[] args) {
        Rectangle patrat1 = new Rectangle(5,5);
        System.out.println("Aria: " + patrat1.calculeazaAria());
        System.out.println("Perimetru: " + patrat1.calculeazaPerimetru());

        System.out.println();

        Rectangle dreptunghi = new Rectangle(12,6);
        System.out.println("Aria: " + dreptunghi.calculeazaAria());
        System.out.println("Perimetru: " + patrat1.calculeazaPerimetru());

    }
}
