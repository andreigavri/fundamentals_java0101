package com.sda.FundamentalsCoding.Contructor;
/**
Create a class called StaticTest with one INSTANCE VARIABLE "name" which is a string
The class should also have a STATIC variable which counts the number of instances created
--> In constructor increment the static field because we want to count the number of instances created by that constructor
Implement the following methods:
- getName() - getter for the instance variable "name"
- getNumOfInstances() - a method that return the static variable (as you may have guessed, the method needs to be static)
Test code:
StaticTest firstInstance = new StaticTest("1st Instance");
System.out.println(firstInstance.getName() + " is instance number " + StaticTest.getNumInstances());
StaticTest secondInstance = new StaticTest("2nd instance");
System.out.println(secondInstance.getName() + " is instance number " + StaticTest.getNumInstances())
 */

public class StaticTestEx {
   String name; // variabila de instanta
   static int nr; // variabila statica

public StaticTestEx(String name){
    this.name = name;
    nr++; //nr tine valoarea a numarului de obiecte create
}
public String getName(){
    return name;

}
public static int getNr(){
    return nr;
}
}

