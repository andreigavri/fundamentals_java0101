package com.sda.FundamentalsCoding.Contructor;
/*
Create a class called "Airport" with attributes for airportCode and city.
Implement a constructor that takes in values for airportCode and city and
initializes the corresponding attributes. Include a method called "welcomePassengers()"
that simply prints a welcome message to the passengers.
 */

public class Airport { // pasul 1 declaram variabilele de instanta
    int airportCode;
    String city;

    public Airport(int airportCode, String city) { // pasul 2 creem constructorul
        this.airportCode = airportCode;
        this.city = city;

    }

    public void welcomePassengers() { //pasul 3 creem metoda specifica clasei Airport
        System.out.println("Bine ati venit in:  " + city + " " + airportCode);
    }

    public static void main(String[] args) {

        //pasul 4 creem obiectele si setam valorile

        Airport airport1 = new Airport(32, "Bucuresti");
        airport1.welcomePassengers();
        Airport airport2 = new Airport(21, "Sibiu");
        airport2.welcomePassengers();
    }
}
