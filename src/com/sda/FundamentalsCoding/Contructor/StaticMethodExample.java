package com.sda.FundamentalsCoding.Contructor;

public class StaticMethodExample {
    int num;
    public static void method1(){
    }
    // metoda statica
    public static void print(){
        // num = 5; -> Caracetristica 2: nu au acces la variabilele non-statice
        //  method1(); -> Nu se poate apela direct o metoda non statica
        method1();
    }
}
