package com.sda.FundamentalsCoding.Contructor;
/*

Create a class called "Product" with instance variables for name, price, and quantity.
Implement two constructors: one that takes only the name and price,
and another constructor that takes all three parameters.

 */

public class Product {

    String name;
    int price;
    int quantity;

    public Product(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public Product(String name, int price, int quantity) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    public static void main(String[] args) {
        Product product1 = new Product("Sampon", 19, 500);
        Product product2 = new Product("Crema de maini", 15);
        System.out.println("Produsul 1: " + product1.name);
        System.out.println("Produsul 2: " + product2.name);

    }
}
