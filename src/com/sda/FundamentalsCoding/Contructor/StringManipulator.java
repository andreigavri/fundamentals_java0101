package com.sda.FundamentalsCoding.Contructor;
/*
Create a class called "StringManipulator" with a method named "concatenate" that concatenates two strings and returns the result.
Overload the "concatenate" method to handle three strings and concatenate them together.
 */

import java.sql.SQLOutput;

public class StringManipulator {

    public static String concatenate(String s1, String s2){
        return s1.concat(s2);
    }

    public static String concatenate(String s1,String s2,String s3){
        String result = s2.concat(s3);
        //s1.concat(s2).concat(s3)
        return result.concat(s1);

    }

    public static void main(String[] args) {
        System.out.println(concatenate("Ana", "Mere"));
        System.out.println(concatenate("Vasile","este","pompier"));
    }
}
