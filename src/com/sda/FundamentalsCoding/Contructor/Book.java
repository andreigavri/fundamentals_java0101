package com.sda.FundamentalsCoding.Contructor;
/*
Create a class called "Book" with instance variables for title, author, and year.
Implement two constructors: one that takes only the title and author,
 and another constructor that takes all three parameters.
 */

public class Book {

    String title;
    String author;
    int year;

    public Book (String title, String author){

        this.title = title;
        this.author = author;


    }

    public Book (String title, String author, int year){

        this.title = title;
        this.author = author;
        this.year = year;
    }

    public static void main(String[] args) {
        Book book1 = new Book("Capra cu 3 iezi","Ion Creanga");
        Book book2 = new Book("Poezii", "Mihai Eminescu", 1968);
    }
}
