package com.sda.FundamentalsCoding.Contructor;

public class Car {
    String color;
    int maxSpeed;
    String brand;
    // Constructor
//    public Car() {
//        this.color = "white";
//        this.maxSpeed = 190;
//        this.brand = "Fiat";
//    }
    public Car() {
    }
    public Car(String color) {
        this.color = color;
    }
    public Car(String color, int maxSpeed, String brand) {
        this.color = color;
        this.maxSpeed = maxSpeed;
        this.brand = brand;
    }
    public void afiseazaDetalii(){
        System.out.println("Color: " + color);
        System.out.println("Max speed: " + maxSpeed);
        System.out.println("Brand: " + brand);
    }
    public static void main(String[] args) {
        // default constructor = cel definit de compilatorul din Java
        Car car1 = new Car();
        car1.afiseazaDetalii();
        car1.brand = "Ford";
        System.out.println();
        car1.afiseazaDetalii();
        System.out.println();
        Car car2 = new Car("Red");
        car2.afiseazaDetalii();
    }
}












