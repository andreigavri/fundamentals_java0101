package com.sda.FundamentalsCoding.Contructor;

public class StaticTest {
    public static void main(String[] args) {
        // Caracteristica 1: nu necesita o instanta a clasei
        // am apelat-o direct
        StaticMethodExample.print();
    }
}