package com.sda.FundamentalsCoding.Contructor;

/* Create a class called "Job" with attributes for title, company, and salary.
        Implement a constructor that takes in values for title, company, and salary and initializes the corresponding attributes.
        Create an object of the Job class using the constructor, passing in values for title, company, and salary.
        Print the details of the job.

 */
public class Job {

    String title;
    String company;
    Double salary;

    public Job( String title, String company, Double salary){
        this.company = company;
        this.title = title;
        this.salary = salary;
    }
    public void detaliiJb(){
        System.out.println("Companie : " + company);
        System.out.println("Titlu : " + title);
        System.out.println("Salariu : " + salary);

    }

    public static void main(String[] args) {
        Job programator = new Job("Programator", "Amazon", 4500.35);
        programator.detaliiJb();

        System.out.println("---------");

        Job constructor = new Job("Constructor", "La negru", 3500.02);
        constructor.detaliiJb();
    }
}
