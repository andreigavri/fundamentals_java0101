package com.sda.FundamentalsCoding.Contructor;
/*
Create a class called "Dog" with attributes for name, breed, and age.
Implement a constructor that takes in values for name, breed, and age and initializes the corresponding attributes.
Create an object of the Dog class using the constructor, passing in values for name, breed, and age. Print the details of the dog.
 */

public class Dog {
    String name;
    String breed;
    int age;


    public Dog(String name, String breed, int age) {
        this.name = name;
        this.breed = breed;
        this.age = age;
    }

    public void print() {
        System.out.println("Name: " + name);
        System.out.println("Breed: " + breed);
        System.out.println("Age: " + age);
    }
    public static void main(String[] args) {

        Dog dog1 = new Dog("Rex", "Golden Retriver", 2);
        dog1.print();
        Dog dog2 = new Dog("Rei", "Labrador", 3);
        dog2.print();
    }
}
