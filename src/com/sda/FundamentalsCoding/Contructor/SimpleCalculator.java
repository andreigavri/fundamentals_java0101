package com.sda.FundamentalsCoding.Contructor;

// Method overloading
// Nu conteaza modificatorul de access (daca e public, private)
// Nu conetaza tipul returnat : int, long
public class SimpleCalculator {
    public int add(int numA, int numB) {
        return numA + numB;
    }

    //    private long add(int numA, int numB){
//        return  numA+numB;
//    }
    // Method overloading prin numarul argumentelor (parametrilor)
    private long add(long numA, long numB, long numC) {
        return numA + numB + numC;
    }

    // Method overloading prin tipul argumentelor (parametrilor)
    public int add(short numA, short numB) {
        return numA + numB;
    }

    // Method overloading prin ordinea argumentelor (parametrilor)
    // !! DOAR DACA SUNT DIFERITE TIPURI DE DATE
    // Ordinea tipurilor de date
    public void method1(String s1, int a) {
    }

    public void method1(int a, String s1) {

    }
}









