package com.sda.FundamentalsCoding.Contructor;

/*
Create a class called "Person" with the following attributes: name, age, and occupation.
 Add a method called "introduce" that prints a message introducing the person with their name, age, and occupation.
 Create an object of the Person class and call the "introduce" method.
 */

public class Person {

    String name;
    int age;
    String occupation;

    public Person (String name, int age, String occupation) {
        this.name = name;
        this.age = age;
        this.occupation = occupation;

    }
    public void introducingPerson(){
        System.out.println("Name:  " + name +  "  " + "Age " + " " + age + " " +  "Ocuppation: "  + " " + occupation);

    }

    public static void main(String[] args) {
        Person person1 = new Person("Eduardo", 32, "Professor");
        person1.introducingPerson();

    }
}
