package com.sda.FundamentalsCoding;

/*
     Write a method that calculates the final score after a level in a game is completed
     The method should return:
      - score
      And the method also takes 3 parameters as input:
     - score which is an int
     - levelCompleted which is an int
     - bonus which is an int

     The method calculate the score by using the following formula: score + (levelCompleted * bonus)
     The method should also print the final score to the console.
  */

public class FinalScore {

    public static int calculateFinalScore(int score, int levelCompleted, int bonus) {
        int finalScore = score + (levelCompleted * bonus);
        return finalScore;
    }


    public static void printScore() {
        int result = calculateFinalScore(15, 7, 5);
        System.out.println("Scorul este: " + result);

    }

    public static void main(String[] args) {
        printScore();

    }
}

