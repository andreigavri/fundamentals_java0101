package com.sda.FundamentalsCoding.StringEx;

/**
 *
 * Create a method "reverseWord" that takes a String as input
 * and returns the reversed version of that String.
 * Use this method in main.
 */

public class ReverseWord {

    public static String reverseWord(String s){
        StringBuilder stringBuilder = new StringBuilder(s);
        stringBuilder.reverse();
        return stringBuilder.toString();
    }
    public static void main(String[] args) {
        System.out.println(reverseWord("avaJ"));
    }
}
