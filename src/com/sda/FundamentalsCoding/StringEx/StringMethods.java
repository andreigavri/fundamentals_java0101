package com.sda.FundamentalsCoding.StringEx;

public class StringMethods {
    public static void main(String[] args) {
        //length
        String s = "elephant";
        int length = s.length();
        System.out.println("length is: " + length);

        //charAt(index)
        char c = s.charAt(0);
        //ultima pozitie: 7
        System.out.println("Char: " + c );

        //substring()
        String subString = s.substring(4);
        System.out.println("Substring: " + subString);

        String subString2 = s.substring(0,2);
        System.out.println("Substring 2: " + subString2);

        //touppercase
        String stringToUpperCase = s.toUpperCase();
        System.out.println("String to upper case: " + stringToUpperCase);

        //equals
        boolean result = s.equals("mere");
        System.out.println("Result: " + result);
        // indexOf
        int i = s.indexOf("h");
        System.out.println ("i: " + i);
        
    }
}
