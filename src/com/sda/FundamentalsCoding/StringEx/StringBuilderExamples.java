package com.sda.FundamentalsCoding.StringEx;

public class StringBuilderExamples {
    public static void main(String[] args) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Hello");
        // Asa se printeaza StringBuilder-ul
        System.out.println(stringBuilder.toString());
        stringBuilder.append(" World!");
        System.out.println(stringBuilder.toString());
        stringBuilder.append(5);
        System.out.println(stringBuilder.toString());
        stringBuilder.insert(3, "Text ");
        System.out.println(stringBuilder.toString());
    }
}