package com.sda.FundamentalsCoding.StringEx;

public class StringIumutabil {
    public static void main(String[] args) {
        String str1 = "Hello";
        System.out.println("str1: " + str1);
//        //Imutabilitate
//        str1.concat("Wolrd");
//        System.out.println("str1: " + str1);
        String str2 = str1.concat("World!");
        System.out.println("str2: " + str2);

    }
}
