package com.sda.FundamentalsCoding.StringEx;
/*
Word Count
Write a Java method called countWords that takes a sentence
as input and returns the number of words in the sentence.
Assume that words are separated by spaces.
 */

public class Count {
    public static int countWords (String s1){
        // Ana are mere
        //Verificam daca string ul nu are continut.

        if(s1 == null || s1.isEmpty()){
            System.out.println("Caz de eroare. ");
            return -1;// returnam -1 de obicei cand sunt cazuri de eroare
        }
        String[] cuvinte = s1.split(" "); // cautam printr un array dupa spatile goale pentru a vedea numarul de cuvinte
        int numarCuvinte = 0;
        for(String cuvant : cuvinte){
            if(!cuvant.isEmpty()){
                numarCuvinte++;

            }
        }
        return numarCuvinte;
    }
    public static void main(String[] args) {

        System.out.println(countWords("Ana are mere"));

    }
}
