package com.sda.FundamentalsCoding.StringEx;

/**
 * 1. Write a Java program to convert a given string into lowercase: “THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG”.
 *   Output: the quick brown fox jumps over the lazy dog
 *
 */
public class StringLowerCase {
    public static void main(String[] args) {
        String s1 = "THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG";
        System.out.println(s1.toLowerCase());
    }
}
