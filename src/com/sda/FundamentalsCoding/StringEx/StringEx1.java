package com.sda.FundamentalsCoding.StringEx;

/*Write a Java method to create the concatenation of the two strings except removing the first
        character of each string. The length of the strings must be 1 and above: “Java” and
        “Fundamentals”
        Output: avaundamentals

 */


public class StringEx1 {
    public static String concatenate(String s1, String s2){
        String concatenare = null;

        if(s1.length() >= 1 && s2.length()>=1){
            String substring1 = s1.substring(1);
            String substring2 = s2.substring(1);

            concatenare = substring1.concat(substring2);
        }
        return concatenare;
    }

    public static void main(String[] args) {
        System.out.println(concatenate("Java","Fundamentals"));
    }
}
