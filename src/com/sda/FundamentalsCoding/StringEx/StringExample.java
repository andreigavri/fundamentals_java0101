package com.sda.FundamentalsCoding.StringEx;

public class StringExample {
    public static void main(String[] args) {
        //equals()
        // ==
        String string1 = new String("Hello");
        String string2 = "Hello";
        //pentrumetoda equals
        //Compar folosindu-ma de metoda equals
        boolean equalsResult = string1.equals(string2); // true
        // Compar folosindu-ma de "=="
        boolean equalityResult = string1 == string2; //  false
        System.out.println("Resultatul la equals() este: " + equalsResult);
        System.out.println("Resultatul la == este: " + equalityResult);
    }
}
