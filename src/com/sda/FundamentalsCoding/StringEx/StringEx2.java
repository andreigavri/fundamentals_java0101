package com.sda.FundamentalsCoding.StringEx;
/*Write a Java program to extract the first half of a string of even length: “Programmer”
        Output: Progr

 */

public class StringEx2 {
    public static String extract(String s1) {
        //Verificam ca stringul contine un numar par de litere , daca nu e par returnam un mesaj de eroare
        String extractie = null;
        if (s1.length() % 2 == 0) {
            System.out.println("Are numar par de litere");
            extractie = s1.substring(0, s1.length() / 2); // De la 0 (primul caracter) pana la jumatatea stringului
        } else {
            System.out.println("Eroare");// Daca intra in else , valoarea initiala o sa fie returnata (null)
        }
        return extractie;
    }

    public static void main(String[] args) {
        System.out.println(extract("Programmer"));
    }
}
