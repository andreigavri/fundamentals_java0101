package com.sda.FundamentalsCoding.Methods;
/*
Write a Java program that checks if a given number is a "teen" number. A teen number is defined as an integer that falls within the range of 13 to 19 (inclusive).

Your task is to implement a Java class called TeenNumberChecker with the following specifications:

1. Implement a method named isTeen that takes an integer as a parameter and returns a boolean value.
This method should check if the number falls within the range of 13 to 19 and return true if it is a teen number, or false otherwise.

2. In the main method, prompt the user to enter a number using the Scanner class.

3. Call the isTeen method with the entered number as an argument and store the result in a variable.

4. If the result is true, display a message stating that the entered number is a teen number.
If the result is false, display a message stating that the entered number is not a teen number.

5. Compile and run the program to test it with various input values.

Example output:

Enter a number: 16
16 is a teen number.

Enter a number: 9
9 is not a teen number.

 */


import java.util.Scanner;

public class TeenNumber {
    public static boolean isTeen(int number) {
        boolean result = false; // plecam de la permisa ca toate celelate varinate sunt false

        if (number >= 13 && number <= 19) { // intre 13 si 19
            result = true;
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner ss = new Scanner(System.in);
        System.out.println("Introduceti numarul: ");
        int number = ss.nextInt();
        boolean result = isTeen(number);  // salvam intr o variabila " RESULT" de TIP "BOOLEAN", valoarea returnata de metoda isTeen,,
        if (result == true) {
            System.out.println(" The entered number is a teen number");

        } else {
            System.out.println(" The entered number is not teen number");
        }
    }
}
