package com.sda.FundamentalsCoding;

/*
*Write a Java program that prompts the user to enter two numbers and performs the following operations using assignment operators:
Assign the sum of the two numbers to a variable.
Assign the difference between the two numbers to a variable.
Assign the product of the two numbers to a variable.
Assign the quotient of the first number divided by the second number to a variable.
 */

import java.util.Scanner;

public class Operations {
    public static void main(String[] args) {
        //Prima cerinta
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduceti primul numar: ");
        int nr1 = scanner.nextInt();

        System.out.println("Introduceti al doilea numar: ");
        int nr2 = scanner.nextInt();

        int suma = nr1 + nr2;
        System.out.println("Suma este:" + suma);

        // cerinta 2

        int diferenta = nr1 - nr2;
        System.out.println("Diferenta este:" + diferenta);

        // cerinta 3

        int produs = nr1 * nr2;
        System.out.println("Produs este: " + produs);

        // cerinta 4

        double impartire = (double) nr1 / nr2;
        System.out.println("Impartire este: " + impartire);

    }
}
