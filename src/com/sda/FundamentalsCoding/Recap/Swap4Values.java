package com.sda.FundamentalsCoding.Recap;

/*
Write a program to swap two variables: a = 5, b = 13.
Output:
Before swapping:
a = 5
b = 13
c = 7
d = 9
After swapping:
a = 13
b = 5
c = 9
d = 7

 */
public class Swap4Values {
    public static void main(String[] args) {
        int a = 5;
        int b = 13;
        int c = 7;
        int d = 9;

        System.out.println("Before swapping");
        System.out.println(" a " + a + " b " + b + " c " + c + " d " + d);
        int temp = a;
        a = b;
        b = temp;
//Optiunea 1
//        int temp2 = c;
//        c = d;
//        d = temp2;

        //Optinuea 2
        temp = c;
        c = d;
        d = temp;

        System.out.println("After swapping");
        System.out.println(" a " + a + " b " + b + " c " + c + " d " + d);
    }
}
