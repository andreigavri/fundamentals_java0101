package com.sda.FundamentalsCoding.Recap;
/*
Write a program to swap two variables: a = 5, b = 13.
Output:
Before swapping:
a = 5
b = 13
After swapping:
a = 13
b = 5

 */
public class Swap {
    public static void main(String[] args) {
        int a = 5;
        int b = 13;
        System.out.println("Before swaping");
        System.out.println(a+ "," + b);

        int temp = a; // pentru valori intermediare
        a = b; // acum a are valoarea 13
        b = temp; // ... b are valoarea intiala a lui a adica 5

        System.out.println("After swaping");
        System.out.println(a+ "," + b);

    }
}
