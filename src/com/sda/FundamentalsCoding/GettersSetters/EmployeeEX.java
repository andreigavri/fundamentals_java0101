package com.sda.FundamentalsCoding.GettersSetters;

public class EmployeeEX {
    public static void main(String[] args) {
        Employee employee1 = new Employee("Vlad",35, 3500);

        String name = employee1.getName();
        int age = employee1.getAge();
        double salary = employee1.getSalary();

        System.out.println("Angajatul are numele: " + name + " Varsta " + age + " Salary " + salary);
        employee1.setName("Cosmin");
        employee1.setAge(43);
        employee1.setSalary(4000);

        System.out.println("Angajatul 1 dupa modificari are numele: " + employee1.getName() + " si varsta: " + employee1.getAge() + " si Salariu " + employee1.getSalary());
    }
}
