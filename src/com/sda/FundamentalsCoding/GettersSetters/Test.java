package com.sda.FundamentalsCoding.GettersSetters;

public class Test {
    public static void main(String[] args) {
        // Voi crea un obiect de tip Person
        Person person1 = new Person("Daniel", 25);
        // Ma folosesc de getteri pentru a accesa variabilele
        String nume = person1.getName();
        int varsta = person1.getAge();
        System.out.println("Persoana 1 are numele: " + nume + " si varsta: " + varsta);
        // Modificam valorile
        person1.setName("Alexandru");
        person1.setAge(29);
        // Trebuie accesate din nou
        // Pt a le printa, noi trebuie sa le accesam
        System.out.println("Persoana 1 dupa modificari are numele: " + person1.getName() + " si varsta: " + person1.getAge());
    }
}

