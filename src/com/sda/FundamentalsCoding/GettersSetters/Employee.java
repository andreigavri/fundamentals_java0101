package com.sda.FundamentalsCoding.GettersSetters;
/*
Create a class called "Employee" with attributes for name, age, and salary.
 Implement the necessary getter and setter methods for all attributes.
  Create an object of the Employee class, set values for name, age, and salary using the setter methods,
  and then print the values using the getter methods.
 */

public class Employee {

    private String name;
    private int age;
    private double salary;

    public Employee(String name, int age, int salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    //Getters pt name
    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public double getSalary() {
        return salary;
    }

    public void setName(String name) {
    }

    public void setAge(int age) {

    }

    public void setSalary(double salary) {

    }
}



