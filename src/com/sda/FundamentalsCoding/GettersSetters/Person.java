package com.sda.FundamentalsCoding.GettersSetters;

public class Person {
    // variabile de instanta
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    // Vom crea getter-ul pentru variabila de instanta "name"
    public String getName() {
        return name;
    }

    // Pt variabila age
    public int getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }
}

