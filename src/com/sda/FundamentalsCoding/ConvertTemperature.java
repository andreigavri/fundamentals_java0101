package com.sda.FundamentalsCoding;

/*
 * Write a Java method that takes a temperature in Fahrenheit as input and converts it to Celsius.
 * The formula to convert Fahrenheit to Celsius is: C = (F - 32) * 5/9.
 * Implement the method and test it by converting a temperature of 98.6 degrees Fahrenheit to Celsius.
 * */
public class ConvertTemperature {
    public static double convertTemperature(double fahrenheit) {
        double celsius = (fahrenheit - 32) * 5 / 9;
        return celsius;

    }

    public static void main(String[] args) {
        double result = convertTemperature(98.6);
        System.out.println(" Temperatura in Celsius este: " + result);
        double result2 = convertTemperature(73.3);
        System.out.println(" Temperatura in Celsius este: " + result2);

    }
}
