package com.sda.FundamentalsCoding;

/*

Write a Java program that prompts the user to enter three numbers and performs the following operations:

Find the maximum among the three numbers.
Calculate the average of the three numbers using arithmetic operators.
Note: decide the correct data types: int, short, double, etc.
 */

import java.util.Scanner;

public class AvarageAndMax {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Primul numar : ");

        int nr1 = scanner.nextInt();
        System.out.println("Al doilea numar : ");

        int nr2 = scanner.nextInt();
        System.out.println("Al treilea numar : ");

        int nr3 = scanner.nextInt();

        //Media = > Avarage of three numbers
        double avarage = (double) (nr1 + nr2 + nr3) / 3.0;//castam
        System.out.println("Avarage : " + avarage);
        //Maximul a 3 numere

        int max = nr1;

        if (nr2 > max) {
            max = nr2;
        }
        if (nr3 > max) {
            max = nr3;
        }
        System.out.println("Maxim : " + max);
    }
}
