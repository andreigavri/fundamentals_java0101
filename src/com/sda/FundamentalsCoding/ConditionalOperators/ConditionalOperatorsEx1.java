package com.sda.FundamentalsCoding.ConditionalOperators;

/*
Write a Java program that prompts the user to enter a character and checks the following conditions using logical operators:

Check if the character is an uppercase letter.
Check if the character is a lowercase letter.
Check if the character is a digit.
Check if the character is a special symbol (not a letter or digit).

 */

import java.util.Scanner;

public class ConditionalOperatorsEx1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Add the character: ");
        char c = scanner.next().charAt(0); // asa putem introduce de la tastatura atat litere cat si cifre (litere, cifre, simboluri...)

        if (c >= 'A' && c <= 'Z') { // asa verificam daca litera caracterul este litera mare (de la A pana la Z)
            System.out.println("Uppercase letter: ");

        } else if (c >= 'a' && c <= 'z') {
            System.out.println("Lowercase letter: ");
        } else if (c >= '0' && c <= '9') {
            System.out.println("Digit:" + c);


        } else {
            System.out.println("Special Symbol: " + c);

        }
    }
}
