package com.sda.FundamentalsCoding.ConditionalOperators;

public class ConditionalOperators {

    public static void main(String[] args) {
        // Conditional AND -> &&
        // takes two boolean arguments
        // returns a boolean result
        // TRUE -> when both arguments are true

        boolean a = true;
        boolean b = true;
        boolean result = a && b;
        System.out.println(result);

        // CONDITIONAL OR -> ||
        // takes two boolean arguments
        // returns a boolean result
        // It is TRUE when AT LEAST one of its arguments is true

        boolean c = false;
        boolean d = true;
        boolean result2 = c || d;
        System.out.println(result2);



        // Logical NOT => Negation
        boolean e = true;
        boolean f = !e;
        System.out.println(f);


    }
}