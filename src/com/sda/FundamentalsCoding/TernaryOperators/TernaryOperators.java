package com.sda.FundamentalsCoding.TernaryOperators;

import java.util.Scanner;

public class TernaryOperators {
    public static void main(String[] args) {

                // Example 1
//        1. Write a Java program that uses a ternary operator to determine whether a given number is even or odd.
//        Display the result accordingly.

                Scanner scanner = new Scanner(System.in);
                System.out.println("Introduceti numarul: ");
                int number = scanner.nextInt();

                if (number % 2 == 0) {
                    System.out.println("Par");
                } else {
                    System.out.println("Impar");
                }

                String result = (number % 2 == 0) ? "Par" : "Impar";
                System.out.println("Numarul este: " + result);


                // Example 2
                // 2. Create a Java program that uses a ternary operator to find the maximum value among 2 given integers and prints the result.
                int num1 = 10;
                int num2 = 15;

                String result2 = (num1 > num2) ? "Numarul 1 este mai mare" : "Numarul doi este mai mare";
                System.out.println(result2);


            }
        }




