package com.sda.FundamentalsCoding.TernaryOperators;
/*
Create a Java program that uses a ternary operator to check if a given character is a vowel or a consonant.
 Display the result accordingly.
 */

import java.util.Scanner;

public class TernaryOperators3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduceti litera: ");
        char c = scanner.next().charAt(0); // 0 - indexul zero/ primul caracter

        String character = (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') ? "Este vocala" : "Este consoana";
        System.out.println(character);
    }
}
