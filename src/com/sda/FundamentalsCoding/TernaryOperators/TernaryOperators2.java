package com.sda.FundamentalsCoding.TernaryOperators;
/*Write a Java program that uses a ternary operator to check if a given year is a leap year.
        Display "Leap year" if it is, and "Not a leap year" otherwise.
        Note: A year is a leap year if it satisfies one of the following conditions:
        The year is divisible by 4 but not divisible by 100.
        The year is divisible by 400.

 */

import java.util.Scanner;

public class TernaryOperators2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduceti an: ");
        int year = scanner.nextInt();

        String result = ((year % 4 == 0 && year% 100 != 0) || (year % 400 == 0))? "Leap year" : "Not Leap year";
        System.out.println(result);

    }
}
